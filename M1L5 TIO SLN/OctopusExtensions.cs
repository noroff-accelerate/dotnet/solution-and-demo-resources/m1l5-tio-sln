﻿using M1L2_TIO_Solutions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M1L5_TIO_SLN
{
    static class OctopusExtensions
    {
        public static void LandCrawl(this Octopus octopus)
        {
            Console.WriteLine($"This octopus located at {octopus.Location} just crawled on land");
        }
    }
}
