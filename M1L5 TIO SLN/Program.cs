﻿using M1L2_TIO_Solutions;
using System;

namespace M1L5_TIO_SLN
{
    class Program
    {
        static void Main(string[] args)
        {
            // Try it out! Create an Athlete class. Give it Id, FirstName, LastName and IsInjured attributes.
            // Give them each a default value.
            // Add a conditional boolean attribute called CanTrain which will be True if IsInjured is False.
            // See Athlete Class in Solution Explorer
            Athlete athlete = new Athlete();
            athlete.IsInjured = false;
            if (athlete.CanTrain)
            {
                Console.WriteLine("Training....Done!");
            }
            else
            {
                Console.WriteLine("Nope athlete is injured");
            }

            //Try it out!Write the name of each attribute to the console of an Athlete object using the nameof feature.
            Console.WriteLine($"{nameof(athlete.Id)}, {nameof(athlete.FirstName)}, {nameof(athlete.LastName)}, {nameof(athlete.IsInjured)}, {nameof(athlete.CanTrain)}");

            // Try it out! Import the DLL of your Animal class project and extend any of the classes to have a new behaviour of your choice.
            GiantOctopus giantOctopus = new GiantOctopus();
            giantOctopus.LandCrawl();

            // Try it out! Make one of the Athlete class's attributes nullable and then use it in a conditional if statement using HasValue.
            // Create two instances one with the attribute of null and one with a value.
            // You can also experiment with using the in line null checking to display the Athletes attributes without worrying about null reference exceptions.
            Athlete athlete1 = new Athlete();
            athlete1.IsInjured = null;
            Athlete athlete2 = new Athlete();
            athlete2.IsInjured = false;

            if (athlete1.IsInjured.HasValue)
            {
                Console.WriteLine("Valid value");
            }
            else
            {
                Console.WriteLine("Probably null");
            }

            if (athlete2.IsInjured.HasValue)
            {
                Console.WriteLine("Valid value");
            }
            else
            {
                Console.WriteLine("Probably null");
            }

            Console.WriteLine($"{athlete1?.IsInjured ?? false} {athlete2?.IsInjured ?? false}");

        }
    }
}
