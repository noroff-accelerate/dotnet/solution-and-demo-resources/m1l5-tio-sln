﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M1L5_TIO_SLN
{
    class Athlete
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool? IsInjured { get; set; }
        public bool CanTrain => (IsInjured==false);
    }
}
